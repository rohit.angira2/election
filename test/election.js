//truffle will get our contract
var Election = artifacts.require("./Election.sol");

//inject all of our account
contract("Election",function(accounts){

//first we test that we do our contract created two candidate or not
//it mocha
//assertion from chai

    it("initilizes with two candidats",function(){
        return Election.deployed().then(function(instance){
            return instance.candidatesCount();
        }).then(function(count){
            assert.equal(count,2);
            });
        });

    var electionInstance;

    it("it initiizes with correct values",function(){
        return Election.deployed().then(function(instance){
            electionInstance = instance;
            return electionInstance.candidates(1);
        }).then(function(candidate){

//            testing first candidate
            assert.equal(candidate[0],1,'contains correct id ');
            assert.equal(candidate[1],'rohit','contains correct name');
            assert.equal(candidate[2],0,'contains correct vote count');
            return electionInstance.candidates(2);
        }).then(function(candidate){
            // testing second candidate
            assert.equal(candidate[0],2,'contains correct id ');
            assert.equal(candidate[1],'mohit','contains correct name');
            assert.equal(candidate[2],0,'contains correct vote count');

        });
    });

    it("allows a voter to cast a vote",function(){
            return Election.deployed().then(function(instance){
                electionInstance = instance;
                candidateId = 1;
                return electionInstance.vote(candidateId,{from:accounts[0]});
            }).then(function(recipt){
                return electionInstance.voters(accounts[0]);
            }).then(function(voted){
               assert(voted,"the voter was marked as voted");
               return electionInstance.candidates(candidateId);
            }).then(function(candidate){
                var votecount = candidate[2];
                assert(votecount , 1 ,"increment the candidate vote count");
            })
    });

    it("throws an exception for invalid candiates", function() {
    return Election.deployed().then(function(instance) {
      electionInstance = instance;
      return electionInstance.vote(99, { from: accounts[1] })
    }).then(assert.fail).catch(function(error) {
      assert(error.message.indexOf('revert') >= 0, "error message must contain revert");
      return electionInstance.candidates(1);
    }).then(function(candidate1) {
      var voteCount = candidate1[2];
      assert.equal(voteCount, 1, "candidate 1 did not receive any votes");
      return electionInstance.candidates(2);
    }).then(function(candidate2) {
      var voteCount = candidate2[2];
      assert.equal(voteCount, 0, "candidate 2 did not receive any votes");
    });
  });

});
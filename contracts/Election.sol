pragma solidity ^0.4.0;

contract Election {
//now we will struct candidate to have multiple information

    struct Candidate{
        uint id;
        string name;
        uint voteCount;
    }
    //store accounts that hve voted
    mapping(address => bool) public voters;
    //store candidate
    //fetch candidate
    //store candidate count solidity does not provide len funation on any struct
    mapping(uint => Candidate) public candidates;
    uint public candidatesCount;

    function Election() public{
        addCandidate("rohit");
        addCandidate("mohit");
    }
    //_name is local variable
    function addCandidate(string _name) private{
        candidatesCount ++;
        candidates[candidatesCount] = Candidate(candidatesCount,_name,0);
    }

    function vote(uint _candidateId) public{
//        accoutn that is voting

        require(!voters[msg.sender]);
        require(_candidateId >0 && _candidateId <= candidatesCount);


        voters[msg.sender] = true;
        candidates[_candidateId].voteCount ++;
    }
}


//if u changing code trufflr migrate --reset